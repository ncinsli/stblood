﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Indicate : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.layer != 8)
        {
            GameObject selectedObject = col.gameObject;

            float sX = selectedObject.transform.position.x;
            float sY = selectedObject.transform.position.y;
            float sZ = selectedObject.transform.position.z;

            selectedObject.transform.position = Vector3.Lerp(new Vector3(sX, sY, -0.25f), selectedObject.transform.position, 0.8f);
        }
    }

    private void OnTriggerExit2D(Collider2D col)
    {
        GameObject selectedObject = col.gameObject;

        float sX = selectedObject.transform.position.x;
        float sY = selectedObject.transform.position.y;
        float sZ = selectedObject.transform.position.z;

        selectedObject.transform.position = new Vector3(sX, sY, 0);

    }

}
