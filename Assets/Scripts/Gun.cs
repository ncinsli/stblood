﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;

public class Gun : MonoBehaviour
{
    public GameObject bulletEtalon;
    private Vector3 mouseView;
    private Vector3 directP2M;
    private Transform playerT;


    private int side;
    // Start is called before the first frame update
    void Start()
    {
        playerT = GameObject.FindGameObjectWithTag("Player").transform;
    }

    private int isPositive(float num)
    {
        int result = num > 0 ? result = 1 : result = -1;
        return result;
    }

    private void FixedUpdate()
    {
        side = isPositive(playerT.localScale.x);


        mouseView = new Vector3(Input.mousePosition.x, Input.mousePosition.y * side, Input.mousePosition.z);
        directP2M = mouseView - transform.position;
        transform.rotation = Quaternion.LookRotation(directP2M) * Quaternion.Euler(0, -90, 0);

        

    }

    private void Update()
    {

        if (Input.GetMouseButtonDown(0))
        {
            GameObject newBullet = Instantiate(bulletEtalon);
            newBullet.transform.position = transform.position;
        }
    }

  
}
