﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogController : MonoBehaviour
{
    private GameObject textObj;
    private Text textComponent;


    // Start is called before the first frame update
    void Start()
    {
        textObj = gameObject;
        textComponent = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void printMessage(string value)
    {
        //Просто ставит текст и прозрачность
        textComponent.text = value;
        textComponent.enabled = true;
        textComponent.color = new Color(textComponent.color.r, textComponent.color.g, textComponent.color.b, 100);
        try{
            GetComponentInChildren<Image>().enabled = true; //для доп. декораций
        }catch { }
    }

    public void cleanMessageBox() {
        //нужно, чтобы убрать текст подсказки
        textComponent.text = "";
        textComponent.enabled = false;
        try{
            GetComponentInChildren<Image>().enabled = false;
        }catch { }
    }


}
