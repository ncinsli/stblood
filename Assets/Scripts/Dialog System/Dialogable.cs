﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dialogable : MonoBehaviour
{
    [SerializeField] private GameObject MessageBox;
    DialogController dgScript;
    [SerializeField] private string Hint = "{E} - использовать";

    // Start is called before the first frame update
    void Start()
    {
        dgScript = MessageBox.GetComponent<DialogController>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        GameObject touchedObject = collision.gameObject;

        if (touchedObject.CompareTag("Player"))
        { //Работает при наличии на объекте триггера
            dgScript.printMessage(Hint);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        GameObject exitedObject = collision.gameObject;

        if (exitedObject.CompareTag("Player"))
        { //Работает при наличии на объекте триггера
            dgScript.cleanMessageBox();
        }
    }
}
