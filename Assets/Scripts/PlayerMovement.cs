﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour{
    [Range(0, 3)] public float speed=1f;
    private Animator playerAnim;
    private Rigidbody2D playerRb;
    private Collider2D playerCol;
    private float scaleX;
    private float scaleY;



    void Awake()
    {
        Application.targetFrameRate=75;    
    }



    void Start(){
        playerAnim = GetComponent<Animator>();
        playerRb = GetComponent<Rigidbody2D>();
        playerCol = GetComponent<Collider2D>();
        scaleX = transform.localScale.x;
        scaleY = transform.localScale.y;

    }

    void Update()
    {

        Debug.DrawRay(Input.mousePosition, transform.position);

        if (Input.GetKeyUp(KeyCode.A))
            playerAnim.SetInteger("AnimationId", 0);
        if (Input.GetKeyUp(KeyCode.D))
            playerAnim.SetInteger("AnimationId", 0);
        if (Input.GetKeyUp(KeyCode.S))
            playerAnim.SetInteger("AnimationId", 0);
        if (Input.GetKeyUp(KeyCode.W))
            playerAnim.SetInteger("AnimationId", 0);
        //Если это не поместить в Update, то анимация при передвижении на небольшое расстояние будет бесконечной
    }

    void FixedUpdate(){





        if (Input.GetKey(KeyCode.A)){
            playerRb.position -= new Vector2(speed, 0);
            transform.localScale = new Vector2(-scaleX, scaleY);
            playerAnim.SetInteger("AnimationId", 1);
        }

        if (Input.GetKey(KeyCode.D)){
            playerRb.position += new Vector2(speed, 0);
            transform.localScale = new Vector2(scaleX, scaleY);
            playerAnim.SetInteger("AnimationId", 1);
        }

        if (Input.GetKey(KeyCode.W)){
            playerRb.position += new Vector2(0, speed);
            playerAnim.SetInteger("AnimationId", 3);
        }

        if (Input.GetKey(KeyCode.S)){
            playerRb.position -= new Vector2(0, speed);
            playerAnim.SetInteger("AnimationId", 2);
        }


    }
}
