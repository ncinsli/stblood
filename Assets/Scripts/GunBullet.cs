﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunBullet : MonoBehaviour
{
    private Vector3 mouseView;
    private Vector3 directP2M;
    private Rigidbody2D brb;
    private Transform playerT;
    private float scaleX;
    private int side;


    void Start()
    {

        playerT = GameObject.FindGameObjectWithTag("Player").transform;
        scaleX = playerT.localScale.x;


        mouseView = new Vector2(Input.mousePosition.x, isPositive(scaleX)*Input.mousePosition.y);
        directP2M = mouseView- transform.position;
        brb = GetComponent<Rigidbody2D>();

    }

    private int isPositive(float num)
    {
        int result = num > 0 ? result = 1 : result = -1;
        return result;
    }


    void FixedUpdate()
    {

        brb.position += (Vector2)directP2M.normalized*Time.deltaTime*3f*scaleX/3;

        // Debug.DrawRay(transform.position, Input.mousePosition); //USE IN CASE OF DEBUGGING
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(!collision.gameObject.CompareTag("Bullet")) Destroy(gameObject);
    }
}
