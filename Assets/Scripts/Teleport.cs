﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Teleport : MonoBehaviour
{
    [Header("References")]
    [SerializeField] private Image overlayImg;
    [Header("Options")]
    [SerializeField] private bool needsClick;
    [SerializeField] private KeyCode whatKeyToClick=KeyCode.E;
    [SerializeField] private string sceneName;

   

    private IEnumerator smoothDark()
    {
        float rColor = overlayImg.color.r;
        float gColor = overlayImg.color.g;
        float bColor = overlayImg.color.b;

        for (int i = 0; i < 100; i++) {
            overlayImg.color = new Color(rColor, gColor, bColor, i*0.02f);

            yield return new WaitForSeconds(0.01f);
        }
        SceneManager.LoadScene(sceneName);

    }


    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("Player")){


            if (needsClick && Input.GetKey(whatKeyToClick)){
                StartCoroutine("smoothDark");
            }
            else if (!needsClick){
                StartCoroutine("smoothDark");
            }
        }
    }

}
